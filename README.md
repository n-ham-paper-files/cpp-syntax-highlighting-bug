Bug with c++ syntax highlighting on Gitlab
=====

I have stumbled across what I am pretty sure is a bug with c++ syntax highlighting on Gitlab. Note that Gitlab syntax highlighting is using [Rogue Rubygem](https://rubygems.org/gems/rouge) so it is quite possible that the bug is there. 

Note I have also:
1. Posted about the problem on the Gitlab forums (see [here](https://forum.gitlab.com/t/possible-bug-with-c-syntax-highlighting/19501));
2. Posted about the problem on the Gitlab subreddit (see [here](https://old.reddit.com/r/gitlab/comments/9kdq28/bug_with_c_syntax_highlighting/)); and
3. Filed an issue on the [Gitlab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce) repository on Gitlab (see [here](https://gitlab.com/gitlab-org/gitlab-ce/issues/52081)).

The bug first appeared when I uploaded `algorithms.h` to Gitlab. From what I can tell the problems begin on line 468 with a red 'if', and then on line 469 it gets confused about what is and is not a string midway through ") {$A_", even though ") {$A_" is handled correctly on line 467.

The files `algorithms-c.h`, `algorithms-cpp.h`, `algorithms-d.h` and `algorithms-java.h` have syntax highlighting for c, cpp, d and java respectively as specified in the `.gitattributes` file. The bug is present for c and cpp, and not present for d and java. 

I have copied the offending code into separate files `bug.cc`, `bug-cpp.cc`, `no-bug.cc` and `no-bug-cpp.cc`. 
1. `bug.cc` and `bug-cpp.cc` have the offending code simply by itself, with `.gitattributes` specifying that `bug-cpp.cc` should use cpp syntax highlighting. Both `bug.cc` and `bug-cpp.cc` present the same problems/bug as before with syntax highlighting (a red 'if' and getting confused about what is and is not a string midway through the second instance of ") {$A_"; and
2. `no-bug.cc` and `no-bug-cpp.cc` have the offending code in an otherwise empty main function, with `.gitattributes` specifying that `no-bug-cpp.cc` should use cpp syntax highlighting. Neither `no-bug.cc` and `no-bug-cpp.cc` present any problems with syntax highlighting.

I find this surprising, if the bug was to not appear in any of the files I would have expected it to be in `bug.cc` where it would be understandable for the syntax highlighting used to not be c++ (as there is no includes or main function to work out what language is being used). 
