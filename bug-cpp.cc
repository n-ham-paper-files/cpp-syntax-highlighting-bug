
    for(auto step=orderedSteps.begin(); step != orderedSteps.end(); step++, i++)
    {
        ofs << "    \\fill (" <<  step->second.first << "," << step->second.second << ") circle( 0.15);" << std::endl;
        ofs << "    \\draw[ultra thick] (0,0)--(" << step->second.first  << "," << step->second.second << ");" << std::endl;
        if(step->second.first >= 0 && step->second.second >= 0)
            ofs << "    \\node () at (" <<  step->second.first + 0.3 << "," <<  step->second.second + 0.3 << ") {$A_" <<  i << "$};" << std::endl;
        else if(step->second.first < 0 && step->second.second >= 0)
            ofs << "    \\node () at (" <<  step->second.first - 0.3 << "," <<  step->second.second + 0.3 << ") {$A_" <<  i << "$};" << std::endl;
        else if(step->second.first < 0 && step->second.second < 0)
            ofs << "    \\node () at (" <<  step->second.first - 0.3 << "," <<  step->second.second - 0.3 << ") {$A_" <<  i << "$};" << std::endl;
        else if(step->second.first >= 0 && step->second.second < 0)
            ofs << "    \\node () at (" <<  step->second.first + 0.3 << "," <<  step->second.second - 0.3 << ") {$A_" <<  i << "$};" << std::endl;
    }

    for(auto step=orderedSteps.begin(); step != orderedSteps.end(); step++, i++)
    {
        ofs << "    \\fill (" <<  step->second.first << "," << step->second.second << ") circle( 0.15);" << std::endl;
        ofs << "    \\draw[ultra thick] (0,0)--(" << step->second.first  << "," << step->second.second << ");" << std::endl;
        if(step->second.first >= 0 && step->second.second >= 0)
            ofs << "    \\node () at (" <<  step->second.first + 0.3 << "," <<  step->second.second + 0.3 << ") {$A_" <<  i << "$};" << std::endl;
        else if(step->second.first < 0 && step->second.second >= 0)
            ofs << "    \\node () at (" <<  step->second.first - 0.3 << "," <<  step->second.second + 0.3 << ") {$A_" <<  i << "$};" << std::endl;
        else if(step->second.first < 0 && step->second.second < 0)
            ofs << "    \\node () at (" <<  step->second.first - 0.3 << "," <<  step->second.second - 0.3 << ") {$A_" <<  i << "$};" << std::endl;
        else if(step->second.first >= 0 && step->second.second < 0)
            ofs << "    \\node () at (" <<  step->second.first + 0.3 << "," <<  step->second.second - 0.3 << ") {$A_" <<  i << "$};" << std::endl;
    }

